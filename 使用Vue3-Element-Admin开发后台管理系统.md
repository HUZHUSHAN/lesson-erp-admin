# 使用 Vue3-Element-Admin 开发后台管理系统

## 初始化项目

```bash
# 克隆项目
git clone https://github.com/huzhushan/vue3-element-admin.git erp  # 克隆到erp文件夹

# 进入项目目录
cd erp

# 安装依赖
npm install

# 启动服务
npm start
```

## 登录

注意点

- 登录接口地址修改
- 登录返回的数据格式
  - 可能不叫 token 和 refresh_token
  - 使用 refresh_token 刷新 token 的接口地址修改
  - 可能没有 refresh_token
- 用户信息接口地址修改
- 用户信息接口返回的数据格式

## 菜单配置

- 新增用户管理、商品管理、权限管理等菜单
- 删除测试页面菜单

## 用户管理

### 用户列表

### 添加用户

### 编辑用户

### 删除用户
