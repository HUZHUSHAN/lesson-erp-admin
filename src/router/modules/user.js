/*
 *           佛曰:
 *                   写字楼里写字间，写字间里程序员；
 *                   程序人员写程序，又拿程序换酒钱。
 *                   酒醒只在网上坐，酒醉还来网下眠；
 *                   酒醉酒醒日复日，网上网下年复年。
 *                   但愿老死电脑间，不愿鞠躬老板前；
 *                   奔驰宝马贵者趣，公交自行程序员。
 *                   别人笑我忒疯癫，我笑自己命太贱；
 *                   不见满街漂亮妹，哪个归得程序员？
 *
 * @Descripttion:
 * @version:
 * @Date: 2021-05-11 11:13:58
 * @LastEditors: huzhushan@126.com
 * @LastEditTime: 2021-05-11 17:23:18
 * @Author: huzhushan@126.com
 * @HomePage: https://huzhushan.gitee.io/vue3-element-admin
 * @Github: https://github.com/huzhushan/vue3-element-admin
 * @Donate: https://huzhushan.gitee.io/vue3-element-admin/donate/
 */
const Layout = () => import('@/layout/index.vue')
const UserList = () => import('@/views/user/index.vue')
const UserAdd = () => import('@/views/user/Add.vue')

export default [
  {
    path: '/user',
    component: Layout,
    name: 'users',
    meta: {
      title: '用户管理',
    },
    icon: 'user',
    children: [
      {
        path: 'list',
        name: 'userList',
        component: UserList,
        meta: {
          title: '用户管理',
        },
      },
      {
        path: 'add',
        name: 'userAdd',
        component: UserAdd,
        meta: {
          title: '添加用户',
        },
        hidden: true,
      },
      {
        path: 'edit/:id',
        name: 'userEdit',
        component: UserAdd,
        meta: {
          title: '编辑用户',
        },
        hidden: true,
      },
    ],
  },
]
