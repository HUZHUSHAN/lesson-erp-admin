/*
 *           佛曰:
 *                   写字楼里写字间，写字间里程序员；
 *                   程序人员写程序，又拿程序换酒钱。
 *                   酒醒只在网上坐，酒醉还来网下眠；
 *                   酒醉酒醒日复日，网上网下年复年。
 *                   但愿老死电脑间，不愿鞠躬老板前；
 *                   奔驰宝马贵者趣，公交自行程序员。
 *                   别人笑我忒疯癫，我笑自己命太贱；
 *                   不见满街漂亮妹，哪个归得程序员？
 *
 * @Descripttion:
 * @version:
 * @Date: 2021-05-11 11:24:31
 * @LastEditors: huzhushan@126.com
 * @LastEditTime: 2021-05-11 11:25:28
 * @Author: huzhushan@126.com
 * @HomePage: https://huzhushan.gitee.io/vue3-element-admin
 * @Github: https://github.com/huzhushan/vue3-element-admin
 * @Donate: https://huzhushan.gitee.io/vue3-element-admin/donate/
 */

const Layout = () => import('@/layout/index.vue')
const RightsList = () => import('@/views/rights/index.vue')

export default [
  {
    path: '/rights',
    component: Layout,
    name: 'rights',
    meta: {
      title: '权限管理',
    },
    icon: 'rights',
    children: [
      {
        path: '',
        name: 'rightsList',
        component: RightsList,
        meta: {
          title: '权限管理',
        },
      },
    ],
  },
]
